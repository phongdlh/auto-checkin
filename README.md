# Auto check in

Automatically daily check in

## Sytem required

- Nodejs
- Taiko
- pm2

## Installation

- Nodejs

```
sudo apt install nodejs
```

- Taiko and pm2

```
sudo npm install -g taiko pm2 --unsafe-perm --allow-root
```

- Auto-checkin
  - Download or clone this repo
  - Go to the root of the repo
  - Run `npm install` or `yarn install`
  - Run `npm run build` or `yarn build`

## Usage

Run

```
npm run auto-checkin start
```

or

```
yarn auto-checkin start
```

Enter your BAP email and password. After creating job for auto checkin successfully, run the following code to check the created job.

```
pm2 list
```

**Note:** this only works for a session. If you want to get the job started on start up, you need follow [this instruction](https://pm2.keymetrics.io/docs/usage/startup/)
