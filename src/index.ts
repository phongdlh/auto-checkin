#!/usr/bin/env node

import { program } from "commander";
import inquirer from "inquirer";
import packageInfo from "../package.json";
import Config from "./utils/config";
import { keepAliveCheckinCron } from "./utils/keep-alive";

program.version(packageInfo.version);

program
  .command("start")
  .description("Start auto check in")
  .action(async () => {
    const answers: { email: string; password: string } = await inquirer.prompt([
      {
        type: "input",
        name: "email",
        message: "Email (youremail@bap.jp):"
      },
      { type: "password", name: "password", message: "Password:" }
    ]);
    new Config(answers);

    console.log("Creating job for auto checkin");
    keepAliveCheckinCron();
    console.log("Create job for auto checkin successfully");
  });

program.parse(process.argv);
