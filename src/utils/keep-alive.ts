import childProcess from "child_process";
import path from "path";

export const keepAliveCheckinCron = () => {
  const CRON_CHECK_IN_FILE_PATH = path.resolve(__dirname, "check-in-cron.js");

  childProcess.execSync(
    `pm2 start ${CRON_CHECK_IN_FILE_PATH} --name checkin --watch `
  );
};