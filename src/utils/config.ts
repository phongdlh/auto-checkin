import Configstore from "configstore";
import packageInfo from "../../package.json";

export default class Config<T = ConfigProps> {
  private static config: Configstore = new Configstore(packageInfo.name);

  constructor(configProps: T = null) {
    if (configProps) {
      Config.config.set(configProps);
    }
  }

  set configObject(configProps: T) {
    Config.config.set(configProps);
  }

  get configObject(): T {
    return Config.config.all;
  }
}
