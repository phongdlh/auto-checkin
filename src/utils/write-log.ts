import fs from "fs";
import path from "path";

const currentDate = new Date();
const day = currentDate.getDate();
const month = currentDate.getMonth() + 1;
const year = currentDate.getFullYear();

const LOG_FILE_PATH = path.resolve(
  __dirname,
  `log-file-${day}-${month}-${year}.log`
);

export const writeLogFile = content => {
  fs.appendFileSync(LOG_FILE_PATH, `${currentDate}:${content}\n`);
};
