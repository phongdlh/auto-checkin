import cron from "node-cron";
import { runCheckIn } from "./auto-checkin";

const CRON_TIME = "10-34/2 8,17 * * *";

const checkInCron = cron.schedule(CRON_TIME, async () => {
  await runCheckIn();
});

checkInCron.start()