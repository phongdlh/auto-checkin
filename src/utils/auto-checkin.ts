import {
  openBrowser,
  goto,
  click,
  write,
  textBox,
  closeBrowser,
  into
} from "taiko";
import notifier from "node-notifier";
import { writeLogFile } from "./write-log";
import Config from "./config";

const config: Config = new Config();
const { email, password }: ConfigProps = config.configObject;

const checkIn = async () => {
  try {
    await openBrowser();
    await goto("https://checkin.bap.jp");
    await click("Log in with Identity℠");
    await write(email, into(textBox({ placeholder: "Username" })));
    await write(password, into(textBox({ placeholder: "Password" })));
    await click("Login and continue");
    await click(email);
    await click("Accept");
    await click("Claim for presence", {
      waitForNavigation: true,
      navigationTimeout: 10000,
      clickCount: 3,
      waitForStart: 2000
    });
  } catch (error) {
    throw error;
  } finally {
    closeBrowser();
  }
};

export const runCheckIn = async () => {
  let errorContent: Error = null;
  const currentDate = new Date();
  const start = new Date();

  try {
    await checkIn();
  } catch (error) {
    errorContent = error;

    notifier.notify({
      title: "Check in error",
      message: `An error occurs at ${currentDate}
      ${error.message}
      `
    });
  } finally {
    const end = new Date();
    const totalTime: number = end.getTime() - start.getTime();

    if (errorContent) {
      return writeLogFile(errorContent);
    }

    return writeLogFile(
      `Check in successfully at ${currentDate}. Time to check in: ${totalTime}`
    );
  }
};
